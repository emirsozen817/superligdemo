<?php
class Calculate
{
    public static function calculateMatch($mainteam,$mainteam_goal,$away_goal,$awayteam)
    {
        require('dbconfig.php');
        $sql = "SELECT * FROM scorboard WHERE teamName IN ('$mainteam');";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $mainteam_array['Id'] = $row['id'];
            $mainteam_array['Team Name'] = $row['teamName'];
            $mainteam_array['Week'] = $row['weeks'];
            $mainteam_array['Win'] = $row['win'];
            $mainteam_array['Draw'] = $row['draw'];
            $mainteam_array['Lose'] = $row['lose'];
            $mainteam_array['Scored Goal'] = $row['scoredGoal'];
            $mainteam_array['Defeated Goal'] = $row['defeatedGoal'];
            $mainteam_array['Average'] = $row['average'];
            $mainteam_array['Score'] = $row['score'];
        }
        } else {
            echo "0 results". "<br>";
        }
        $sql = "SELECT * FROM scorboard WHERE teamName IN ('$awayteam');";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $awayteam_array['Id'] = $row['id'];
            $awayteam_array['Team Name'] = $row['teamName'];
            $awayteam_array['Week'] = $row['weeks'];
            $awayteam_array['Win'] = $row['win'];
            $awayteam_array['Draw'] = $row['draw'];
            $awayteam_array['Lose'] = $row['lose'];
            $awayteam_array['Scored Goal'] = $row['scoredGoal'];
            $awayteam_array['Defeated Goal'] = $row['defeatedGoal'];
            $awayteam_array['Average'] = $row['average'];
            $awayteam_array['Score'] = $row['score'];
        }
        } else {
            echo "0 results". "<br>";
        }

        if($mainteam_goal > $away_goal)
        {
            $mainteam_array['Score'] = +3;
            $awayteam_array['Score'] = 0;

            $mainteam_array['Week'] = +1;
            $awayteam_array['Week'] = +1;

            $mainteam_array['Win'] = +1;
            $mainteam_array['Lose'] = 0;

            $mainteam_array['Draw'] = 0;
            $awayteam_array['Draw'] = 0;

            $awayteam_array['Win'] = 0;
            $awayteam_array['Lose'] = +1;

            $mainteam_array['Scored Goal'] = +$mainteam_goal;
            $awayteam_array['Scored Goal'] = +$away_goal;

            $mainteam_array['Defeated Goal'] = +$away_goal;
            $awayteam_array['Defeated Goal'] = +$mainteam_goal;

            $mainteam_array['Average'] = $mainteam_array['Scored Goal'] - $mainteam_array['Defeated Goal'];
            $awayteam_array['Average'] = $awayteam_array['Scored Goal'] - $awayteam_array['Defeated Goal'];
        }
        else if($away_goal > $mainteam_goal) 
        {
            $awayteam_array['Score'] = +3;
            $mainteam_array['Score'] = 0;

            $mainteam_array['Week'] = +1;
            $awayteam_array['Week'] = +1;

            $awayteam_array['Win'] = +1;
            $awayteam_array['Lose'] = 0;

            $mainteam_array['Draw'] = 0;
            $awayteam_array['Draw'] = 0;

            $mainteam_array['Win'] = 0;
            $mainteam_array['Lose'] = +1;

            $mainteam_array['Scored Goal'] = +$mainteam_goal;
            $awayteam_array['Scored Goal'] = +$away_goal;

            $mainteam_array['Defeated Goal'] = +$away_goal;
            $awayteam_array['Defeated Goal'] = +$mainteam_goal;

            $mainteam_array['Average'] = $mainteam_array['Scored Goal'] - $mainteam_array['Defeated Goal'];
            $awayteam_array['Average'] = $awayteam_array['Scored Goal'] - $awayteam_array['Defeated Goal'];
        }
        else if($mainteam_goal == $away_goal)
        {
            $mainteam_array['Score'] = +1;
            $awayteam_array['Score'] = +1;

            $mainteam_array['Week'] = +1;
            $awayteam_array['Week'] = +1;

            $mainteam_array['Win'] = 0;
            $mainteam_array['Lose'] = 0;

            $awayteam_array['Win'] = 0;
            $awayteam_array['Lose'] = 0;

            $mainteam_array['Draw'] = +1;
            $awayteam_array['Draw'] = +1;

            $mainteam_array['Scored Goal'] = +$mainteam_goal;
            $awayteam_array['Scored Goal'] = +$away_goal;

            $mainteam_array['Defeated Goal'] = +$away_goal;
            $awayteam_array['Defeated Goal'] = +$mainteam_goal;

            $mainteam_array['Average'] = $mainteam_array['Scored Goal'] - $mainteam_array['Defeated Goal'];
            $awayteam_array['Average'] = $awayteam_array['Scored Goal'] - $awayteam_array['Defeated Goal'];
        }
        $conn->close();
        return ['MainTeam'=>$mainteam_array, 'AwayTeam'=>$awayteam_array];  
    }
}
?>