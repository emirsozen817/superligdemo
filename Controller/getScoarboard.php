<?php
require('../dbconfig.php');

$sql = "SELECT * FROM scorboard Order BY  score DESC,average DESC,teamName;";
$result = $conn->query($sql);
$i=0;
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $i++;
        $r[$i]['Name']=$row["teamName"];
        $r[$i]['Week']=$row["weeks"];
        $r[$i]['Win']=$row["win"];
        $r[$i]['Draw']=$row["draw"];
        $r[$i]['Lose']=$row["lose"];
        $r[$i]['Scored_Goal']=$row["scoredGoal"];
        $r[$i]['Defeated_Goal']=$row["defeatedGoal"];
        $r[$i]['Average']=$row["average"];
        $r[$i]['Score']=$row["score"];
    }
} else {
    echo "0 results";
}
$conn->close();
print_r(json_encode($r));
?>