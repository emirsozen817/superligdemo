<?php
require('../dbconfig.php');

for ($x = 1; $x <= 5; $x++) {
    $sql = "SELECT * FROM fixture where weeks=$x Order BY weeks";
    $result = $conn->query($sql);
    $i=0;
    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $i++;
            $r[$x][$i]['mainteam']=$row["mainteam"];
            $r[$x][$i]['mainteam_goal']=$row["mainteam_goal"];
            $r[$x][$i]['away_goal']=$row["away_goal"];
            $r[$x][$i]['awayteam']=$row["awayteam"];
            $r[$x][$i]['week']=$row["weeks"];
            $r[$x][$i]['date']=$row["dates"];
            $r[$x][$i]['status']=$row["statuses"];
        }
    } else {
        echo "0 results";
    }
} 

$conn->close();
print_r(json_encode($r));
?>