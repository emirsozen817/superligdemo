<?php
require_once("calculate.php");

function updateBoard_MainTeam($id,$teamname,$week,$win,$draw,$lose,$scored_goal,$defeated_goal,$average,$score)
{
    //Güncel Puan Durumu
    require('dbconfig.php');
    $sql = "UPDATE scorboard SET 
    weeks=weeks+$week,
    win=win+$win,   
    draw=draw+$draw,
    lose=lose+$lose,
    scoredGoal=scoredGoal+$scored_goal,
    defeatedGoal=defeatedGoal+$defeated_goal,
    average=average+$average,
    score=score+$score
    WHERE id=$id";

    if ($conn->query($sql) === TRUE) {
        echo "Record updated successfully" . "<br>";
    } else {
        echo "Error updating record: " . $conn->error;
    }
    $conn->close();

    //Haftanın Kayıdını Al
    require('dbconfig.php');
    $sql = "INSERT INTO weeks (teamName, weeks, win, draw, lose, scoredGoal, defeatedGoal, average, score)
        				SELECT '$teamname', weeks, win, draw, lose, scoredGoal, defeatedGoal, average, score
        				FROM scorboard
           			 WHERE id = $id";
    if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }    
}
function updateBoard_AwayTeam($id,$teamname,$week,$win,$draw,$lose,$scored_goal,$defeated_goal,$average,$score)
{
    //Güncel Puan Durumu
    require('dbconfig.php');
    $sql = "UPDATE scorboard SET 
    weeks=weeks+$week,
    win=win+$win,
    draw=draw+$draw,
    lose=lose+$lose,
    scoredGoal=scoredGoal+$scored_goal,
    defeatedGoal=defeatedGoal+$defeated_goal,
    average=average+$average,
    score=score+$score
    WHERE id=$id";

    if ($conn->query($sql) === TRUE) {
        echo "Record updated successfully";
    } else {
        echo "Error updating record: " . $conn->error;
    }
    $conn->close();
    
    //Haftanın Kayıdını Al
    require('dbconfig.php');
    $sql = "INSERT INTO weeks (teamName, weeks, win, draw, lose, scoredGoal, defeatedGoal, average, score)
        				SELECT '$teamname', weeks, win, draw, lose, scoredGoal, defeatedGoal, average, score
        				FROM scorboard
           			 WHERE id = $id";
    if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }                   
    $conn->close();

}

function allFixture()
{
    require('dbconfig.php');
    $sql = "SELECT * FROM fixture";
    $result = $conn->query($sql);
    $i = 0;
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $results = Calculate::calculateMatch($row['mainteam'],$row['mainteam_goal'],$row['away_goal'],$row['awayteam']);
            updateBoard_MainTeam($results['MainTeam']['Id'],$results['MainTeam']['Team Name'],$results['MainTeam']['Week'],$results['MainTeam']['Win'],$results['MainTeam']['Draw']
            ,$results['MainTeam']['Lose'],$results['MainTeam']['Scored Goal'],$results['MainTeam']['Defeated Goal'],
            $results['MainTeam']['Average'],$results['MainTeam']['Score']);
            updateBoard_AwayTeam($results['AwayTeam']['Id'],$results['AwayTeam']['Team Name'],$results['AwayTeam']['Week'],$results['AwayTeam']['Win'],$results['AwayTeam']['Draw']
            ,$results['AwayTeam']['Lose'],$results['AwayTeam']['Scored Goal'],$results['AwayTeam']['Defeated Goal'],
            $results['AwayTeam']['Average'],$results['AwayTeam']['Score']);
        }
    } else {
        echo "0 results";
    }
    $conn->close();
}
allFixture();
?>