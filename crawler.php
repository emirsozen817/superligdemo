<?php
include('simple_html_dom.php');
function createTeams()
{
    require_once('dbconfig.php');
    $html = file_get_html('http://www.tff.org/Default.aspx?pageID=198&hafta=1#grp');
    foreach($html->find('div#ctl00_MPane_m_198_1890_ctnr_m_198_1890_Panel1 table') as $team) {
    for ($pr_id = 1; $pr_id <= 18; $pr_id++) {
        $name  = $team->find('#ctl00_MPane_m_198_1890_ctnr_m_198_1890_grvACetvel_ctl'.sprintf("%02d", $pr_id).'_lnkTakim', 0)->plaintext;
        $splitText = preg_replace("/[0-9]+/", "", $name);
        $item['teamName']= substr($splitText, 1);
        $results[] = $item;
        } 
    }
    foreach($results as $db){
    $sql = "INSERT INTO teams (teamName) VALUES ('$db[teamName]')";
        if (mysqli_query($conn, $sql)) {
        echo "Successfully"."<br>";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }
    }
    mysqli_close($conn);
}

function createFixture()
{

     $html = file_get_html('http://www.tff.org/Default.aspx?pageID=198&hafta=20#macctl00_MPane_m_198_935_ctnr_m_198_935');
     foreach($html->find('#ctl00_MPane_m_198_935_ctnr_m_198_935_dtlHaftaninMaclari') as $i)
     {
        for ($pr_id = 1; $pr_id <= 9; $pr_id++) {
        $date = $i->find('#ctl00_MPane_m_198_935_ctnr_m_198_935_dtlHaftaninMaclari_ctl'.sprintf("%02d", $pr_id).'_lblTarih', 0)->plaintext;
        $items['date'] = str_replace(".","-",$date);
        $items['mainteam'] = $i->find('#ctl00_MPane_m_198_935_ctnr_m_198_935_dtlHaftaninMaclari_ctl'.sprintf("%02d", $pr_id).'_Label4', 0)->plaintext;
        $items['mainteam_goal'] = $i->find('#ctl00_MPane_m_198_935_ctnr_m_198_935_dtlHaftaninMaclari_ctl'.sprintf("%02d", $pr_id).'_Label5', 0)->plaintext;
        $items['away_goal'] = $i->find('#ctl00_MPane_m_198_935_ctnr_m_198_935_dtlHaftaninMaclari_ctl'.sprintf("%02d", $pr_id).'_Label6', 0)->plaintext;
        $items['awayteam'] = $i->find('#ctl00_MPane_m_198_935_ctnr_m_198_935_dtlHaftaninMaclari_ctl'.sprintf("%02d", $pr_id).'_Label1', 0)->plaintext;
        $week = $html->find('#ctl00_MPane_m_198_935_ctnr_m_198_935_hs_Tab2',0)->plaintext;
        $test = preg_replace("/[^0-9]/", "",$week);
        $items['week'] = $test;
         if($items['mainteam_goal'] == ' ')
             $items['status'] = 'false';
         else
             $items['status'] = 'true';
         if($items['mainteam_goal'] == ' ')
            $items['mainteam_goal'] = 'NULL';
         if($items['away_goal'] == ' ')
            $items['away_goal'] = 'NULL';
        $results[] = $items;
        }
     }
     print_r($results);

     function insertFixture(array $array)
     {
         require_once('dbconfig.php');
         foreach($array as $a)
         {
            $sql = "INSERT INTO fixture (mainteam,mainteam_goal,away_goal,awayteam,weeks,dates,statuses) 
            VALUES ('$a[mainteam]',$a[mainteam_goal],$a[away_goal],'$a[awayteam]','$a[week]','$a[date]','$a[status]')";
            if (mysqli_query($conn, $sql)) {
            echo "Successfully"."<br>";
            } else {
                echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            }
         }  
         mysqli_close($conn);
     }
     return insertFixture($results);
}

function createLeague()
{
    require_once('dbconfig.php');
    $html = file_get_html('http://www.tff.org/Default.aspx?pageID=198&hafta=01#macctl00_MPane_m_198_935_ctnr_m_198_935');
    foreach($html->find('div#ctl00_MPane_m_198_1890_ctnr_m_198_1890_Panel1 table') as $e) {
    for ($pr_id = 1; $pr_id <= 18; $pr_id++) {
     $name  = $e->find('#ctl00_MPane_m_198_1890_ctnr_m_198_1890_grvACetvel_ctl'.sprintf("%02d", $pr_id).'_lnkTakim', 0)->plaintext;
     $splitText = preg_replace("/[0-9]+/", "", $name);
     $item['teamName']   = substr($splitText, 1);
     $item['week']   = 0;
     $item['win']   =  0;
     $item['draw']   = 0;
     $item['lose']   = 0;
     $item['scoredGoal']   = 0;
     $item['defeatedGoal']   = 0;
     $item['average']   = 0;
     $item['score']   = 0;
     $result[] = $item;
     } 
    }
    foreach($result as $db){
    $sql = "INSERT INTO scorboard (teamName ,weeks ,win ,draw, lose, scoredGoal , defeatedGoal , average , score)
    VALUES ('$db[teamName]',$db[week],$db[win],$db[draw],$db[lose],$db[scoredGoal],$db[defeatedGoal],$db[average],$db[score])";
    if (mysqli_query($conn, $sql)) {
        echo "New record created successfully".'<br>';
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }
}
}

print_r(createFixture());
die;
?>